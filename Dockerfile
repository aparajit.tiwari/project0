FROM ubuntu
RUN apt update
RUN apt-get install -y apache2-bin apache2
RUN apt-get install -y apache2-utils
EXPOSE 80
ENTRYPOINT ["apache2ctl"]
CMD ["-DFOREGROUND"]
